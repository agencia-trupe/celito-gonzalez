# humanstxt.org/
# The humans responsible & technology colophon

# TEAM
		Trupe Agência Criativa -- http://trupe.net
    Nilton de Freitas -- Web Developer -- @nildotcom

# TECHNOLOGY COLOPHON
	
		PHP
    HTML5, CSS3
    jQuery, Modernizr
