<div class="conteudo projetos galeria">
    <div class="interna">
        <div class="projetos-tipos">
        <ul>
            <?php foreach ($tipos as $tipo): ?>
                <li><a <?php echo ($projeto_tipo == $tipo->slug) ? 'class="active"' : '' ?> href="<?php echo site_url('projetos/lista/' . $tipo->slug) ?>">/<?php echo $tipo->nome ?></a></li>
            <?php endforeach ?>
        </ul>
    </div>
    </div>
    <div class="projeto-meta-nav-wrapper">
        <div class="interna">
            <div class="projeto-meta">
                <h1><?php echo $projeto->titulo ?></h1>
                <div class="projeto-meta-separador"></div>
                <div class="projeto-meta-descricao">
                    <?php echo $projeto->texto ?>
                </div>
            </div>
            <div class="projeto-nav">
                <div class="nav-container">
                    <?php if(isset($prev)): ?>
                    <?=anchor( 'projetos/detalhe/' . $prev, '<span>anterior</span>', 'class="projeto-anterior"'); ?>
                    <?php endif; ?>
                </div>                
                <div class="nav-container next-container">
                    <?php if(isset($next)): ?>
                    <?=anchor( 'projetos/detalhe/' . $next, '<span>próximo</span>', 'class="projeto-proximo"'); ?>
                    <?php endif; ?>
                </div>
                <div class="grid-container">
                    <a href="<?=site_url( 'projetos/lista/' . $projeto_tipo ); ?>" class="projeto-grid"><span>todos os projetos</span></a>
                </div>
            </div> 
            <div class="clearfix"></div> 
        </div>
    </div>
   
    <div class="projeto-galeria">
        <div id="c-carousel">
            <div id="wrapper">
                <div id="carousel">
                    <?php foreach ( $fotos as $foto ): ?>
                        <img width="800" height="520" src="<?=base_url('assets/img/projetos/fotos/' . $foto->imagem ); ?>" alt="<?=$foto->titulo; ?>">
                    <?php endforeach; ?>
                </div>
                <div id="prev"></div>
                <div id="next"></div>
                <div class="projetos-galeria-controles">
                    <a href="" class="prev"></a>
                    <a href="" class="next"></a>
                </div>
            </div>
        </div> <!-- /c-carousel -->
       
    </div>
</div>