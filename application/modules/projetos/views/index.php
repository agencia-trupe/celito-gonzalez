<div class="conteudo projetos">
    <div class="projetos-tipos">
        <ul>
            <?php if($tipos): ?>
            <?php foreach ($tipos as $tipo): ?>
                <li><a <?php echo ($projeto_tipo == $tipo->slug) ? 'class="active"' : '' ?> href="<?php echo site_url('projetos/lista/' . $tipo->slug) ?>">/<?php echo $tipo->nome ?></a></li>
            <?php endforeach;
                  endif; ?>
        </ul>
    </div>
    <?php if ($projetos):
          foreach ( $projetos as $projeto ): ?>
        <a href="<?=site_url( 'projetos/detalhe/' . $projeto_tipo . '/' . $projeto->id ); ?>" class="projeto-box">
            <img src="<?=base_url( 'assets/img/projetos/capas/' . $projeto->capa ); ?>" 
             alt="<?=$projeto->titulo; ?>">
            <span class="titulo"><?=$projeto->titulo; ?></span>
            <div class="lupa"></div>
        </a>
    <?php endforeach; 
        endif; ?>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>