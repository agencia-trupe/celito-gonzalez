<div class="row-fluid">
    <div class="span9">
        <legend>Mostras 
                <?php echo anchor('painel/mostras/cadastrar', 'Novo', 'class="btn btn-info btn-mini"'); ?>  
                <a href="#" class="ordenar-mostras btn btn-mini btn-info">ordenar mostras</a>
                <a href="#" class="salvar-ordem-mostras hide btn btn-mini btn-warning">salvar ordem</a>
        </legend>
        <div class="alert alert-info hide mostras-mensagem">
            <span>Para ordenar, clique no nome da mostra e arraste até a posição desejada</span>
            <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span6">Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mostras as $mostra): ?>
                <tr id="mostra_<?php echo $mostra->id ?>">
                    <td><?=$mostra->titulo; ?></td>
                    <td><?=anchor('mostras/admin_mostras/editar/' . $mostra->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?=anchor('mostras/admin_mostras/deleta_mostra/' . $mostra->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>