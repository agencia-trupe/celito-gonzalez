<?php
class Midias extends MX_Controller
{
    var $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('midias/midia');
        $this->data['pagina'] = 'midia';
    }
    public function index()
    {
        $this->lista();
    }
    public function lista()
    {
        $this->data['midias'] = $this->midia->get_all();
        $this->data['conteudo'] = 'midias/index';
        $seo = array(
            'titulo' => 'Mídia',
            'descricao' =>  'Celito Gonzalez Arquitetura na Mídia' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }

    public function detalhe($id)
    {
        $this->load->model('midias/clipping');
        $this->load->model('midias/midia');

        $this->data['clipping'] = $this->clipping->get_clipping($id);
        $this->data['conteudo'] = 'midias/detalhe';

        $this->data['prev'] = $this->midia->get_prev($id);
        $this->data['next'] = $this->midia->get_next($id);

        $seo = array(
            'titulo' => 'Mídia',
            'descricao' =>  'Celito Gonzalez Arquitetura na Mídia' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }
}