<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Detalhes SEO
|
| Configurações básicas de SEO.
|--------------------------------------------------------------------------
*/
$config['site_name'] = 'Celito Gonzalez Arquitetura';

$config['site_decription'] = 'Criação de Projetos e Reformas Residencial e Comercial (Corporativo).';

$config['site_keywords'] = 'Celito Gonzalez, Arquitetura, Interiores';

$config['site_author'] = 'Trupe Agência Criativa - http://trupe.net';
/* End of file seo.php */
/* Location: ./application/config/seo.php */