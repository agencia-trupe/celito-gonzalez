<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['perfil'] = 'paginas/view/perfil';
$route['atuacao'] = 'paginas/view/atuacao';
$route['midia'] = 'midias';
$route['midia/detalhe/(:num)'] = 'midias/detalhe/$1';

$route['login']  = 'auth/login';
$route['logout'] = 'auth/logout';
$route['registrar'] = 'auth/register';

$route['painel'] = 'paginas/admin_paginas';
$route['painel/paginas'] = 'paginas/admin_paginas';
$route['painel/projetos'] = 'projetos/admin_tipos';
$route['painel/projetos/(:any)'] = 'projetos/admin_projetos/$1';
$route['painel/mostras'] = 'mostras/admin_mostras';
$route['painel/mostras/(:any)'] = 'mostras/admin_mostras/$1';
$route['painel/servicos'] = 'servicos/admin_servicos';
$route['painel/servicos/(:any)'] = 'servicos/admin_servicos/$1';
$route['painel/dicas'] = 'dicas/admin_dicas';
$route['painel/dicas/(:any)'] = 'dicas/admin_dicas/$1';
$route['painel/contato'] = 'contato/admin_contatos';
$route['painel/contato/(:any)'] = 'contato/admin_contatos/$1';
$route['painel/midia'] = 'midias/admin_midias';
$route['painel/midia/(:any)'] = 'midias/admin_midias/$1';
$route['painel/slideshow'] = 'admin_slideshow';
$route['painel/slideshow/(:any)'] = 'admin_slideshow/$1';
$route['painel/tipos'] = 'projetos/admin_tipos';
$route['painel/tipos/(:any)'] = 'projetos/admin_tipos/$1';

$route['404_override'] = '';





/* End of file routes.php */
/* Location: ./application/config/routes.php */