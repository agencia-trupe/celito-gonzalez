            </div>
            <div class="clearfix"></div>
        </div>
        <footer class="<?php echo $pagina ?>">
            <span class="copy">
                &copy; 2012 Celito Gonzalez &middot; Todos os direitos reservados - Criação de Sites: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa 
                <img src="<?php echo base_url('assets/img/trupe.png') ?>" alt="">
            </span>

        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
         <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?=base_url('assets/prettyphoto/js/jquery.prettyPhoto.js'); ?>"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <?php if ($pagina == 'home'): ?>
        <script>
            runSlider();
        </script>
        <?php endif; ?>
        <script>
            var _gaq=[['_setAccount','UA-35751088-1'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
