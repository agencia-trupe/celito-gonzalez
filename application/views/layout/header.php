<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <?php $this->seo->build_meta(); ?>
        <meta name="viewport" content="width=device-width,
                               maximum-scale=1.0" />
        <link rel="stylesheet/less" href="<?php echo base_url(); ?>assets/css/main.less">
        <script src="<?php echo base_url(); ?>assets/js/vendor/less-1.3.0.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?=base_url('assets/prettyphoto/css/prettyPhoto.css'); ?>">
         <link rel="stylesheet" href="<?=base_url('assets/css/jquery.maximage.css'); ?>">

        <title><?=$this->seo->get_title(); ?></title>
    </head>
    <body>
        <div class="clearfix"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="wrapper">
            <div class="clearfix"></div>
        <header class="<?=$pagina;?>">
            <div class="interna">
                <a href="<?=site_url(); ?>" class="marca left"></a>
                <?php echo Modules::run('contato/sociais'); ?>
                <nav class="right">
                    <ul>
                        <li>
                            <a <?=($pagina == 'home') ? 'class="active"' : ''; ?> 
                                href="<?=site_url(); ?>">Home
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'perfil') ? 'class="active"' : ''; ?> 
                                href="<?=site_url('perfil'); ?>">Perfil
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'atuacao') ? 'class="active"' : ''; ?> 
                                href="<?=site_url('atuacao'); ?>">Atuação
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'projetos') ? 'class="active"' : ''; ?> 
                                href="<?=site_url('projetos'); ?>">Projetos
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'mostras') ? 'class="active"' : ''; ?> 
                                href="<?=site_url('mostras'); ?>">Mostras
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'midia') ? 'class="active"' : ''; ?> 
                                href="<?=site_url('midia'); ?>">Mídia
                            </a>
                        </li>
                        <li>
                            <a <?=($pagina == 'contato') ? 'class="active"' : ''; ?> 
                                href="<?=site_url('contato'); ?>">Contato
                            </a>
                        </li>
                    </ul>
                </nav>

                <div class="clearfix"></div>
            </div>
        </header>
        <div class="barra-inferior"></div>
        <div id="main" class="<?php echo $pagina; ?>">
